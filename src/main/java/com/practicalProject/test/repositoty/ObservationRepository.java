package com.practicalProject.test.repositoty;

import com.practicalProject.test.model.Client;
import com.practicalProject.test.model.Observation;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class ObservationRepository {
    private final SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

    public List<Observation> getObservation(int clientId){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        List<Observation> observationList = session.createQuery("from Observation where client.id =:clientId").setParameter("clientId",clientId).list();

        transaction.commit();
        session.close();

        return observationList;
    }
    public void addObservation(Observation observation){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.save(observation);

        transaction.commit();
        session.close();
    }


}
