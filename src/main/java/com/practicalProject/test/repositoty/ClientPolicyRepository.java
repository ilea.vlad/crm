package com.practicalProject.test.repositoty;

import com.practicalProject.test.model.Client;
import com.practicalProject.test.model.ClientPolicy;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClientPolicyRepository {

    private final SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();


    public List<ClientPolicy> getALLClientPolicy() {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery("from ClientPolicy");
        List<ClientPolicy> clientPolicies = query.list();


        transaction.commit();
        session.close();

        return clientPolicies;
    }

    public void addPolicy(ClientPolicy clientPolicy) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.save(clientPolicy);

        transaction.commit();
        session.close();
    }

    public List<ClientPolicy> getPolicyByClient(int clientID) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery("from ClientPolicy where client.id =: ID");
        query.setParameter("ID", clientID);
        List<ClientPolicy> clientPolicyList = query.list();


        transaction.commit();
        session.close();
        return clientPolicyList;
    }

    public void updateClientPolicy(ClientPolicy clientPolicy) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.update(clientPolicy);

        transaction.commit();
        session.close();
    }

    public List<ClientPolicy> getClientByPolicyNumber(String policyNumber) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        List<Client> clientList = new ArrayList<>();

        List<ClientPolicy> clientPolicyList = session.createQuery("from ClientPolicy where  policyNumber =:policuNumber ").setParameter("policuNumber", policyNumber).list();


        transaction.commit();
        session.close();
        return clientPolicyList;

    }
    public void updatePolicyEuroValue(List<ClientPolicy> clientPolicies){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        for (ClientPolicy clientPolicy: clientPolicies){
            session.update(clientPolicy);
        }

        transaction.commit();
        session.close();
    }


}
