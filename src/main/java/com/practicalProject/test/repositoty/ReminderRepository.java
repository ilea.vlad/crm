package com.practicalProject.test.repositoty;

import com.mysql.cj.xdevapi.Client;
import com.practicalProject.test.model.Reminder;
import com.practicalProject.test.util.SendEmailToClient;
import com.practicalProject.test.util.SendReminderEmail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import sun.rmi.transport.Transport;

import java.util.List;

public class ReminderRepository {
    private final SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

    public List<Reminder> getReminderByClientId(int id){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery("from Reminder where client_id =: idClient");
        query.setParameter("idClient",id);
        List<Reminder> reminderList = query.list();

        transaction.commit();
        session.close();
        return reminderList;
    }

    public void addReminder(Reminder reminder){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.save(reminder);

        transaction.commit();;
        session.close();
    }

    public void updateReminder(Reminder reminder){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.update(reminder);

        transaction.commit();
        session.close();
    }

    public void deleteReminder(Reminder reminder){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.delete(reminder);

        transaction.commit();
        session.close();
    }
    public List<Reminder> getReminders(){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        List<Reminder> reminderList = session.createQuery("from Reminder").list();

        transaction.commit();
        session.close();
        return reminderList;

    }
}
