package com.practicalProject.test.service;

import com.practicalProject.test.model.Client;
import com.practicalProject.test.repositoty.ClientRepository;

import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.List;

public class ClientService {
    private ClientRepository clientRepository = new ClientRepository();

    public void createClient(String name, String address, String phoneNumber, String emailAddress, String cnp, boolean gdpr, boolean lead) {
        Client client = new Client();
        client.setName(name);
        client.setAddress(address);
        client.setPhoneNumber(phoneNumber);
        client.setEmailAddress(emailAddress);
        client.setCnp(cnp);
        client.setGdpr(gdpr);
        client.setFlag(false);
        client.setLead(lead);
        clientRepository.addClient(client);
    }

    public void updateClient(Client client, String name, String address, String phoneNumber, String emailAddress, String cnp, boolean gdpr, boolean lead) {
        client.setName(name);
        client.setAddress(address);
        client.setPhoneNumber(phoneNumber);
        client.setEmailAddress(emailAddress);
        client.setCnp(cnp);
        client.setGdpr(gdpr);
        client.setLead(lead);
        clientRepository.updateClient(client);
    }

    public List<Client> getAllClients() {
        List<Client> clientList = clientRepository.getAllClients();
        return clientList;
    }

    public List<String> getClientsNames() {
        List<Client> clientList = clientRepository.getAllClients();
        List<String> clientsNames = new ArrayList<>();
        for (Client client : clientList) {
            clientsNames.add(client.getName());
        }
        return clientsNames;
    }

    public List<Client> getClientByCNP(String cnp, String name) {
        List<Client> clientList = clientRepository.getClientByCnpOrName(cnp, name);
        return clientList;
    }

//    public List<Client> getClientByName(String name) {
//        List<Client> clientList = clientRepository.getClientByName(name);
//        return clientList;
//    }

    public Client getClient(String name) {
        Client client = clientRepository.getClientByName(name);
        return client;
    }

    public List<Client> getLeadsClients() {
        List<Client> clientList = clientRepository.getLeadsClient();
        return clientList;
    }
    public List<Client> getLeadsByNameOrCnp(String name,String cnp) {
        List<Client> clientList = clientRepository.getLeadByNameOrCnp(cnp,name);
        return clientList;
    }

    public List<Client> getClientWithBirthday() {
        List<Client> clientList = clientRepository.getClientBirthday();
        return clientList;
    }

    public void updateFlag(Client client, boolean flag){
        client.setFlag(flag);
        clientRepository.updateFlag(client);
    }


}
