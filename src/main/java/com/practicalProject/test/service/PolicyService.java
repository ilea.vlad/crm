package com.practicalProject.test.service;

import com.practicalProject.test.model.Policy;
import com.practicalProject.test.repositoty.PolicyRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PolicyService {
    PolicyRepository policyRepository = new PolicyRepository();

    public void addPolicy(String policyName){
        Policy policy = new Policy();
        policy.setPolicyName(policyName);
        policyRepository.addPolicy(policy);
    }

    public void updatePolicy(Policy policy,String policyName){
        policy.setPolicyName(policyName);
        policyRepository.updatePolicy(policy);
    }

    public Policy getPolicyByName(String policyName){
        Policy policy = policyRepository.getPolicyByName(policyName);

        return policy;
    }
    public Set<String> getPolicyTypeByCategory(String category){
        Set<String> policyNames = new HashSet<>();
        for (Policy policy:policyRepository.getPolicyTypeByCategory(category)){
            policyNames.add(policy.getPolicyType());
        }
        return policyNames;
    }
    public List<String> getPolicyNameByType(String type){
        List<String> policyNames = new ArrayList<>();

        for (Policy policy: policyRepository.getPolicyNameByType(type)) {
            policyNames.add(policy.getPolicyName());
        }

        return policyNames;
    }


    public List<String> getPolicyNames(){
       List<Policy> policyList = policyRepository.getAllPolicies();
       List<String> policyNames = new ArrayList<>();
       for (Policy policy:policyList){
           policyNames.add(policy.getPolicyName());
       }
       return policyNames;
    }
}
