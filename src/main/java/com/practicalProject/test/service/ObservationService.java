package com.practicalProject.test.service;

import com.practicalProject.test.model.Client;
import com.practicalProject.test.model.Observation;
import com.practicalProject.test.repositoty.ObservationRepository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public class ObservationService {
    ObservationRepository observationRepository = new ObservationRepository();

    public List<Observation> getObsetvations(int clientId){
        List<Observation> observationList = observationRepository.getObservation(clientId);
        return observationList;
    }

    public void addObservation(Client client, LocalDate date, String message){
        Observation observation = new Observation();
        observation.setClient(client);
        observation.setMessagedate(date);
        observation.setMessage(message);
        observationRepository.addObservation(observation);
    }
}
