package com.practicalProject.test.service;

import com.practicalProject.test.model.Client;
import com.practicalProject.test.model.Reminder;
import com.practicalProject.test.repositoty.ReminderRepository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public class ReminderService {
    ReminderRepository reminderRepository = new ReminderRepository();

    public void addReminder(Client client, String message, LocalDate reminderDate) {
        Reminder reminder = new Reminder();
        reminder.setClientId(client);
        reminder.setMessage(message);
        reminder.setReminderDate(reminderDate);
        reminderRepository.addReminder(reminder);

    }

    public List<Reminder> getReminderByClient(Client client) {
        List<Reminder> reminderList = reminderRepository.getReminderByClientId(client.getClientId());
        return reminderList;
    }

    public void editReminder(Reminder reminder, String message) {
        reminder.setMessage(message);
        reminderRepository.updateReminder(reminder);

    }
    public void deleteReminder(Reminder reminder){
        reminderRepository.deleteReminder(reminder);
    }

    public List<Reminder> getReminders(){
        List<Reminder> reminderList = reminderRepository.getReminders();
        return reminderList;
    }
}
