package com.practicalProject.test.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "observation")
public class Observation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "observation_id")
    private int obervationId;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;

    @Column(name = "message_date")
    private LocalDate messagedate;

    @Column(name = "message")
    private  String message;

    public Client getClient() {
        return client;
    }

    public LocalDate getMessagedate() {
        return messagedate;
    }

    public String getMessage() {
        return message;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setMessagedate(LocalDate messagedate) {
        this.messagedate = messagedate;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
