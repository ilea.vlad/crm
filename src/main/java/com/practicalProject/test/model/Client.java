package com.practicalProject.test.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "client")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "client_id")
    private int clientId;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email_address")
    private String emailAddress;

    @Column(name = "cnp")
    private String cnp;

    @Column(name = "gdpr")
    private Boolean gdpr;

    @Column(name = "lead_status")
    private Boolean lead;

    @Column(name = "flag")
    private Boolean flag;


    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    public void setLead(Boolean lead) {
        this.lead = lead;
    }

    public Boolean isLead() {
        return lead;
    }

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "clientId")
    //@JoinColumn(name = "id_reminder")
    private List<Reminder> reminderList = new ArrayList<>();

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "client")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<ClientPolicy> policyList = new ArrayList<>();

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "client")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Observation> observationList = new ArrayList<>();

    public void setName(String name) {
        this.name = name;
    }

    public int getPolicyList() {
        return policyList.size();
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhoneNumber(String phone_nuber) {
        this.phoneNumber = phone_nuber;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public void setGdpr(Boolean gdpr) {
        this.gdpr = gdpr;
    }

    public int getClientId() {
        return clientId;
    }

    public int getReminderList() {
        return reminderList.size();
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getCnp() {
        return cnp;
    }

    public Boolean isGdpr() {
        return gdpr;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + clientId +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", phone number=" + phoneNumber +
                ", email address=" + emailAddress +
                ", cnp=" + cnp +
                ", gdpr=" + gdpr +
                "}";
    }




}

