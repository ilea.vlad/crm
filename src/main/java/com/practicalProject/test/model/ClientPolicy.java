package com.practicalProject.test.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "client_policy")
public class ClientPolicy {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "client_policy_id")
    private int clientPolicyId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "client_id")
    private Client client;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "policy_id")
    private Policy policy;

    @Column(name = "policy_number")
    private String policyNumber;

    @Column(name = "period")
    private Integer period;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "fee")
    private Integer fee;

    @Column (name = "price_equivalent")
    private Double euroPrice;

    public void setEuroPrice(Double euroPrice) {
        this.euroPrice = euroPrice;
    }

    public Double getEuroPrice() {
        return euroPrice;
    }

    public Client getClient() {
        return client;
    }

    public String getPolicy() {
        return policy.getPolicyName();
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public Integer getPeriod() {
        return period;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public Integer getFee() {
        return fee;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setPolicy(Policy policy) {
        this.policy = policy;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    @Override
    public String toString() {
        return "ClientPolicy{" +
                "clientPolicyId=" + clientPolicyId +
                ", client=" + client +
                ", policy=" + policy +
                ", policyNumber='" + policyNumber +
                ", period=" + period +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", fee=" + fee +
                '}';
    }
}
