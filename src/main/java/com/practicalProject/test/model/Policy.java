package com.practicalProject.test.model;

import javax.persistence.*;

@Entity
@Table(name = "policy")
public class Policy {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "policy_id")
    private int policyId;

    @Column(name = "policy_subtype")
    private String policyName;

    @Column(name = "policy_type")
    private String policyType;

    @Column(name = "policy_category")
    private String policyCategory;

    public String getPolicyCategory() {
        return policyCategory;
    }

    public String getPolicyType() {
        return policyType;
    }

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }


    public String getPolicyName() {
        return policyName;
    }

    @Override
    public String toString() {
        return "Policy{" +
                "policyId=" + policyId +
                ", policyName='" + policyName + '\'' +
                '}';
    }
}
