package com.practicalProject.test.util;

import com.practicalProject.test.model.Client;
import com.practicalProject.test.service.ClientService;

import javax.mail.MessagingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CheckBirthday {

    public static void CnptoDate(List<Client> clientList) throws MessagingException {
        List<Client> clientWithBirthday = new ArrayList<>();
        ClientService clientService = new ClientService();
        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("dd.MM");
        for (Client client : clientList) {

            if (ft.format(date).equals(dataDeNastereDinCNP(client.getCnp()).substring(0, 5))) {
                clientService.updateFlag(client, true);
                clientWithBirthday.add(client);
            }
        }
        if (!clientWithBirthday.isEmpty()) {
            SendEmailToClient.sendMail(clientWithBirthday);
        }
    }

    public static String dataDeNastereDinCNP(String cnp) {
        String zi = cnp.substring(5, 7);
        String luna = cnp.substring(3, 5);
        String an = cnp.substring(1, 3);

        return zi + "." + luna + "." + "19" + an;
    }
}

