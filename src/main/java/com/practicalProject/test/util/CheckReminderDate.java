package com.practicalProject.test.util;


import com.practicalProject.test.model.Reminder;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CheckReminderDate {
    public static List<Reminder> checkReminderDate(List<Reminder> reminderList)  {
        List<Reminder> checkedReminders = new ArrayList<>();
        for (Reminder reminder : reminderList) {
            if (LocalDate.now().isEqual(reminder.getReminderDate())){
                checkedReminders.add(reminder);
            }
        }

        return checkedReminders;

    }
}
