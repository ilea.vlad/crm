package com.practicalProject.test.util;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;

public class GetEuroValue {
    public static double getValue() throws IOException {
        try {
            URL oracle = new URL("http://www.infovalutar.ro/bnr/azi/eur");
            URLConnection yc = oracle.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    yc.getInputStream()));

            return Double.valueOf(in.readLine());
        }catch (UnknownHostException e){
            return 0.00;
        }

    }
}
