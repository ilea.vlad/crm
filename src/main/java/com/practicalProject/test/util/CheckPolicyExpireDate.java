package com.practicalProject.test.util;

import com.practicalProject.test.model.ClientPolicy;
import com.practicalProject.test.model.Policy;


import java.time.LocalDate;
import java.util.ArrayList;

import java.util.List;

public class CheckPolicyExpireDate {
    public static List<ClientPolicy> checkPolicyExpireDate(List<ClientPolicy> clientPolicyList) {
        List<ClientPolicy> expiredPolicies = new ArrayList<>();
        for (ClientPolicy clientPolicy : clientPolicyList) {
            if (clientPolicy.getEndDate().isEqual(LocalDate.now()) & clientPolicy.getEndDate().isBefore(LocalDate.now().plusDays(10))) {
                expiredPolicies.add(clientPolicy);
            }
        }

        return expiredPolicies;
    }
}
