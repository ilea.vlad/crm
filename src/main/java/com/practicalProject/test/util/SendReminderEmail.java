package com.practicalProject.test.util;

import com.practicalProject.test.model.ClientPolicy;
import com.practicalProject.test.model.Reminder;
import com.practicalProject.test.service.ObservationService;
import com.practicalProject.test.service.ReminderService;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class SendReminderEmail {
    public static void sendMail(List<Reminder> reminderList, List<ClientPolicy> clientPolicyList) throws MessagingException {
        Properties properties = new Properties();
        String recipient = "consultanta.ao@gmail.com";
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");

        String myAccountEmail = "service.email.crm@gmail.com";
        String password = "ParolaEmailServiceCrm";
        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(myAccountEmail, password);
            }
        });


        if (!reminderList.isEmpty() || !clientPolicyList.isEmpty()) {
            Message message = prepareMessage(session, myAccountEmail, recipient, reminderList, clientPolicyList);
            Transport.send(message);
        }

    }

    private static Message prepareMessage(Session session, String myAccountEmail, String recipient, List<Reminder> reminderList, List<ClientPolicy> clientPolicyList) throws MessagingException {
        String remindersMessageContent = "";
        String expiredPolicyList = "";
        String finalMessage = "";

        SimpleDateFormat ft = new SimpleDateFormat("dd.MM.yyyy");

        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(myAccountEmail));
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
        message.setSubject("Reminder");

        for (Reminder reminder : reminderList) {
            remindersMessageContent = remindersMessageContent + reminder.getClientId().getName() + ": " + reminder.getMessage() + "\n" + reminder.getClientId().getPhoneNumber() + "\n";
            ObservationService observationService = new ObservationService();
            observationService.addObservation(reminder.getClientId(),reminder.getReminderDate(),"(reminder)"+reminder.getMessage());
            ReminderService reminderService = new ReminderService();
            reminderService.deleteReminder(reminder);
        }
        for (ClientPolicy clientPolicy : clientPolicyList) {
            expiredPolicyList = expiredPolicyList + clientPolicy.getClient().getName() + "\n Policy Number : " + clientPolicy.getPolicyNumber() + ": " + ft.format(clientPolicy.getEndDate()) + "\n";
        }
        finalMessage = "Reminder for today: \n" + remindersMessageContent + "\n\n" +
                "Policies that expire in the next 10 days:\n" + expiredPolicyList;
        message.setText(finalMessage);
        return message;
    }
}
