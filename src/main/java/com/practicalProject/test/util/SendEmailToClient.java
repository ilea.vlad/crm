package com.practicalProject.test.util;

import com.practicalProject.test.model.Client;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import java.util.List;
import java.util.Properties;

public class SendEmailToClient {
    public static void sendMail(List<Client> clientList) throws MessagingException {
        Properties properties = new Properties();

        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");

        String myAccountEmail = "service.email.crm@gmail.com";
        String password = "ParolaEmailServiceCrm";
        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(myAccountEmail,password);
            }
        });

        Message message = prepareMessage(session,myAccountEmail,clientList);

        Transport.send(message);

    }

    private static Message prepareMessage(Session session,String myAccountEmail,List<Client> clientList) throws MessagingException {
        Message message = new MimeMessage(session);
        StringBuilder fullMessage = new StringBuilder();
        message.setFrom(new InternetAddress(myAccountEmail));
        message.setRecipient(Message.RecipientType.TO,new InternetAddress("consultanta.ao@gmail.com"));
        message.setSubject("Birthday Today");
        for (Client client:clientList) {
            fullMessage.append(client.getName() + "\n" + client.getPhoneNumber() + "\n" + client.getEmailAddress()).
                    append("\n---------------------------------------\n");
        }
        message.setText(fullMessage.toString());
        return message;
    }

}
