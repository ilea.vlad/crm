package com.practicalProject.test.view;

import com.practicalProject.test.model.Client;
import com.practicalProject.test.model.Reminder;
import com.practicalProject.test.service.ReminderService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.text.SimpleDateFormat;
import java.util.Date;


public class RemindersView {
    ReminderService reminderService = new ReminderService();
    ObservableList<Reminder> observableList;


    public void showReminders(Client client){
        observableList = FXCollections.observableList(reminderService.getReminderByClient(client));
        Stage showReminderWindow = new Stage();
        showReminderWindow.setHeight(700);
        showReminderWindow.setWidth(600);

        Label clientNameLabel = new Label(client.getName());
        clientNameLabel.setFont(new Font("Arial",40));
        Button addButton = new Button("Add Reminder");
        TextField phoneNumberText = new TextField();
        phoneNumberText.setText(client.getPhoneNumber());
        phoneNumberText.setPrefWidth(100);
        phoneNumberText.setEditable(false);
        TableView reminderTable = new TableView();
        reminderTable.setPrefSize(500,400);

        TableColumn messageColumn = new TableColumn("Message");
        messageColumn.setMinWidth(470);
        TableColumn reminderDateCol = new TableColumn("Date");
        reminderDateCol.setMinWidth(110);

        messageColumn.setCellValueFactory(new PropertyValueFactory<>("message"));
        reminderDateCol.setCellValueFactory(new PropertyValueFactory<>("reminderDate"));

        reminderTable.setItems(observableList);
        reminderTable.getColumns().addAll(messageColumn,reminderDateCol);

        reminderTable.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2){
                Reminder reminder =(Reminder) reminderTable.getSelectionModel().getSelectedItem();
                EditReminder editReminder = new EditReminder();
                editReminder.editReminder(reminder);
                reminderTable.setOnMouseReleased(event1 -> {
                });
                observableList = FXCollections.observableList(reminderService.getReminderByClient(client));
                reminderTable.setItems(observableList);
            }

        });
        addButton.setOnAction(event -> {
            AddReminderView addReminderView = new AddReminderView();
            addReminderView.addReminder(client);

            addButton.setOnMouseReleased(event1 -> {
                observableList = FXCollections.observableList(reminderService.getReminderByClient(client));
                reminderTable.setItems(observableList);
            });
        });

        HBox hBox = new HBox();
        hBox.getChildren().add(phoneNumberText);
        hBox.setPadding(new Insets(0,100,0,100));
        hBox.setAlignment(Pos.CENTER);

        VBox vBox = new VBox();
        vBox.setSpacing(50);
        vBox.setPadding(new Insets(20,0,0,0));
        vBox.setAlignment(Pos.CENTER);
        vBox.getChildren().addAll(clientNameLabel,hBox,addButton,reminderTable);

        Scene scene = new Scene(vBox);
        scene.getStylesheets().add("Viper.css");
        showReminderWindow.setScene(scene);
        showReminderWindow.showAndWait();

    }

}
