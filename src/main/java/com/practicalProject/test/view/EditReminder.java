package com.practicalProject.test.view;

import com.practicalProject.test.model.Reminder;
import com.practicalProject.test.service.ReminderService;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;


public class EditReminder {
    ReminderService reminderService = new ReminderService();

    public void editReminder(Reminder reminder){
        Stage editWindow = new Stage();
        editWindow.setHeight(400);
        editWindow.setWidth(400);
        editWindow.initModality(Modality.APPLICATION_MODAL);

        TextArea messageTextArea = new TextArea();
        messageTextArea.setPrefSize(300,200);
        messageTextArea.setText(reminder.getMessage());
        Button editButton = new Button("Edit");

        editButton.setOnAction(event -> {
            reminderService.editReminder(reminder,messageTextArea.getText());
            editButton.setOnMouseReleased(event1 -> editWindow.close());
        });

        VBox editVBox = new VBox();
        editVBox.setSpacing(20);
        editVBox.setAlignment(Pos.CENTER);
        editVBox.setPadding(new Insets(10,20,20,20));
        editVBox.getChildren().addAll(messageTextArea,editButton);

        Scene scene = new Scene(editVBox);
        scene.getStylesheets().add("Viper.css");
        editWindow.setScene(scene);
        editWindow.showAndWait();
    }
}
