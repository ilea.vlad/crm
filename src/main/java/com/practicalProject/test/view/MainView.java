package com.practicalProject.test.view;

import com.practicalProject.test.model.Client;

import javax.mail.MessagingException;

import com.practicalProject.test.service.ClientPolicyService;
import com.practicalProject.test.service.ClientService;
import com.practicalProject.test.service.ReminderService;
import com.practicalProject.test.util.*;
import com.sun.mail.util.MailConnectException;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.IOException;


public class MainView extends Application {

    private ClientService clientService = new ClientService();
    private ReminderService reminderService = new ReminderService();
    private ClientPolicyService clientPolicyService = new ClientPolicyService();


    private ObservableList<Client> clientObservableList = FXCollections.observableList(clientService.getAllClients());


    public static void main(String[] args) {

        launch();

    }

    public void init() throws MessagingException, IOException {


        try {
            //Sent email to clients at birthday
            CheckBirthday.CnptoDate(clientService.getClientWithBirthday());

            //Sent email to user with reminders
            SendReminderEmail.sendMail(CheckReminderDate.checkReminderDate(reminderService.getReminders()), CheckPolicyExpireDate.checkPolicyExpireDate(clientPolicyService.getAllClientPolicy()));
        } catch (MailConnectException e) {

        }
        //setEuroEquivalent
        clientPolicyService.setEquivalentValue(clientPolicyService.getAllClientPolicy(), GetEuroValue.getValue());

    }

    @Override
    public void start(Stage stage) {

        BorderPane mainPage = new BorderPane();
        stage.setTitle("CRM");
        stage.setWidth(1300);
        stage.setHeight(1000);
        mainPage.setMaxHeight(540);
        mainPage.setMaxWidth(749);


        Label cnpLabel = new Label("CNP:");
        cnpLabel.setFont(new Font("Arial", 20));
        TextField cnpTextField = new TextField();
        Label nameLabel = new Label("Name:");
        nameLabel.setFont(new Font("Arial", 20));
        TextField nameTextField = new TextField();
        Label policyLabel = new Label("Policy:");
        policyLabel.setFont(new Font("Arial", 20));
        TextField policyTextFiled = new TextField();
        Label leadLabel = new Label("Lead");
        leadLabel.setFont(new Font("Arial", 20));
        CheckBox leadCheckBox = new CheckBox();
        leadCheckBox.setPrefSize(50, 50);
        TableView<Client> table = createTable();
        table.setEditable(false);
        table.setPadding(new Insets(0, 0, 0, 10));
        table.setMaxWidth(1017);
        table.setMaxHeight(800);


        //Create Buttons
        Button addButton = new Button("Add Client");
        addButton.setPrefWidth(150);
        addButton.setPrefHeight(100);
        Button updateClientButton = new Button("Update Client");
        updateClientButton.setPrefWidth(150);
        updateClientButton.setPrefHeight(100);
        Button showPoliciesButton = new Button("Show Policies");
        showPoliciesButton.setPrefWidth(150);
        showPoliciesButton.setPrefHeight(100);
        Button showReminderButton = new Button("Show Reminders");
        showReminderButton.setPrefWidth(150);
        showReminderButton.setPrefHeight(100);
        Button searchButton = new Button("Search");
        searchButton.setPrefWidth(100);
        searchButton.setPrefHeight(60);

        final HBox searchHBox = new HBox();
        searchHBox.setSpacing(10);
        searchHBox.setPadding(new Insets(50, 0, 0, 160));
        searchHBox.setAlignment(Pos.CENTER);

        searchHBox.getChildren().addAll(cnpLabel, cnpTextField, nameLabel, nameTextField, policyLabel, policyTextFiled, leadLabel, leadCheckBox, searchButton);
        mainPage.setTop(searchHBox);

        final VBox functionVBox = new VBox();
        functionVBox.setSpacing(20);
        functionVBox.setPadding(new Insets(190, 0, 0, 65));
        functionVBox.getChildren().addAll(addButton, updateClientButton, showPoliciesButton, showReminderButton);
        mainPage.setLeft(functionVBox);
        mainPage.setCenter(table);


        addButton.setOnAction(event -> {
            AddClientView addClientView = new AddClientView();
            try {
                addClientView.addClient();
            } catch (Exception e) {
                e.printStackTrace();
            }
            addButton.setOnMouseReleased(event1 -> table.setItems(refreshObservebleList()));
        });

        table.setOnMouseClicked(event -> {
            Client client = table.getSelectionModel().getSelectedItem();
            if (event.getClickCount() == 2) {
                ShowObservation showObservation = new ShowObservation();
                showObservation.showObservation(client);
            } else {


                updateClientButton.setOnAction(event1 -> {
                    UpdateClientView clientView = new UpdateClientView();
                    clientView.update(client);

                    table.setItems(refreshObservebleList());
                });

                showPoliciesButton.setOnAction(event2 -> {
                    ClientPoliciesView clientPoliciesView = new ClientPoliciesView();
                    clientPoliciesView.ViewClientPolicies(client);
                    table.setItems(refreshObservebleList());
                });

                showReminderButton.setOnAction(event1 -> {
                    RemindersView remindersView = new RemindersView();
                    remindersView.showReminders(client);
                    table.setItems(refreshObservebleList());
                });

            }
        });
        Scene primaryScene = new Scene(mainPage);
        primaryScene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                table.setItems(searchFunction(policyTextFiled.getText(), cnpTextField.getText(),
                        nameTextField.getText(), leadCheckBox.isSelected()));
            }

        });
        searchButton.setOnAction(event -> {
            table.setItems(searchFunction(policyTextFiled.getText(), cnpTextField.getText(),
                    nameTextField.getText(), leadCheckBox.isSelected()));
        });

        primaryScene.getStylesheets().add("Viper.css");

        stage.setScene(primaryScene);
        stage.show();


    }

    private TableView createTable() {
        TableView table = new TableView();
        //Create Table Column
        TableColumn<Client, String> nameCol = new TableColumn<>("Name");
        nameCol.setMinWidth(150);
        TableColumn<Client, String> addressCol = new TableColumn<>("Address");
        addressCol.setMinWidth(200);
        TableColumn<Client, String> phoneNumberCol = new TableColumn<>("Phone Number");
        phoneNumberCol.setMinWidth(140);
        phoneNumberCol.setEditable(true);
        TableColumn<Client, String> emailCol = new TableColumn<>("Email");
        emailCol.setMinWidth(200);
        TableColumn<Client, String> cnpCol = new TableColumn<>("CNP");
        cnpCol.setMinWidth(130);
        TableColumn<Client, Boolean> gdprCol = new TableColumn<>("GDPR");
        gdprCol.setMinWidth(60);
        TableColumn<Client, Integer> reminderCol = new TableColumn<>("REM");
        reminderCol.setMinWidth(15);
        TableColumn<Client, Integer> policyNumber = new TableColumn<>("Policy");
        reminderCol.setMinWidth(15);

        //Table Data Insert
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        addressCol.setCellValueFactory(new PropertyValueFactory<>("address"));
        phoneNumberCol.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        emailCol.setCellValueFactory(new PropertyValueFactory<>("emailAddress"));
        cnpCol.setCellValueFactory(new PropertyValueFactory<>("cnp"));
        gdprCol.setCellValueFactory(new PropertyValueFactory<>("gdpr"));
        reminderCol.setCellValueFactory(new PropertyValueFactory<>("reminderList"));
        policyNumber.setCellValueFactory(new PropertyValueFactory<>("policyList"));


        table.setItems(clientObservableList);
        table.getColumns().addAll(nameCol, addressCol, phoneNumberCol, emailCol, cnpCol, gdprCol, policyNumber, reminderCol);

        return table;
    }


    private ObservableList<Client> refreshObservebleList() {
        return FXCollections.observableList(clientService.getAllClients());
    }

    private ObservableList<Client> searchFunction(String policyNumber, String cnp, String name, boolean lead) {
        if (policyNumber.equals("")) {
            if (cnp.equals("") && name.equals("")) {
                if (lead) {
                    clientObservableList = FXCollections.observableList(clientService.getLeadsClients());
                    return clientObservableList;
                } else {
                    return refreshObservebleList();
                }
            } else {
                if (lead) {
                    clientObservableList = FXCollections.observableList(clientService.getLeadsByNameOrCnp(name, cnp));

                } else {
                    clientObservableList = FXCollections.observableList(clientService.getClientByCNP(cnp, name));
                }
                return clientObservableList;

            }
        } else {
            clientObservableList = FXCollections.observableList(clientPolicyService.getClientsByPolicyNumber(policyNumber));
            return clientObservableList;
        }

    }


}
