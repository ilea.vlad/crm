package com.practicalProject.test.view;

import com.practicalProject.test.model.Client;
import com.practicalProject.test.model.Observation;
import com.practicalProject.test.service.ObservationService;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;


public class ShowObservation {
    ObservationService observationService = new ObservationService();


    public void showObservation(Client client) {
        Stage showObsetvation = new Stage();
        showObsetvation.setHeight(700);
        showObsetvation.setWidth(600);

        Label nameLabel = new Label(client.getName());
        nameLabel.setFont(new Font("Arial", 40));
        Button addLogButton = new Button("Log");
        addLogButton.setMinWidth(100);
        addLogButton.setPrefHeight(100);
        TextArea logsTextArea = new TextArea();
        logsTextArea.setEditable(false);
        TextArea logAddTextField = new TextArea();



        SimpleDateFormat ft = new SimpleDateFormat("dd.MM.yyyy");
        List<Observation> observationList = observationService.getObsetvations(client.getClientId());
        for (Observation observation : observationList) {
           logsTextArea.appendText(observation.getMessagedate() + " : " + observation.getMessage() + "\n");
           logsTextArea.appendText("-------------------------------------------------------------------------------------------\n");
        }

        addLogButton.setOnAction(event -> {
            if (!(logAddTextField.getText().isEmpty())){
                observationService.addObservation(client, LocalDate.now(),logAddTextField.getText());
                logAddTextField.clear();
            }
        });
        VBox logsVBox = new VBox();
        logsVBox.setSpacing(50);
        logsVBox.setPadding(new Insets(20, 0, 0, 0));
        logsVBox.setAlignment(Pos.CENTER);
        logsVBox.getChildren().addAll(nameLabel, logAddTextField, addLogButton, logsTextArea);

        Scene scene = new Scene(logsVBox);
        scene.getStylesheets().add("Viper.css");
        showObsetvation.setScene(scene);
        showObsetvation.showAndWait();


    }


}
