package com.practicalProject.test.view;


import com.practicalProject.test.model.Client;
import java.util.prefs.*;

public class test {
    private static final String DATE = "Date";

    public static void savePreference(String date) {
        Preferences prefs = Preferences.userNodeForPackage(Client.class);

        prefs.put(DATE, date);
    }

    public static String readPreference() {
        Preferences prefs = Preferences.userNodeForPackage(Client.class);

        return prefs.get(DATE, "default");
    }
}






