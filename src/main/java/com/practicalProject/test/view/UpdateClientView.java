package com.practicalProject.test.view;

import com.practicalProject.test.model.Client;
import com.practicalProject.test.service.ClientService;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class UpdateClientView {
    ClientService clientService = new ClientService();


    public void update(Client client) {
        Stage updateWindow = new Stage();
        updateWindow.initModality(Modality.APPLICATION_MODAL);

        updateWindow.setWidth(400);
        updateWindow.setHeight(700);


        //Name field
        Label nameLabel = new Label("Name");
        TextField nameTextField = new TextField();
        nameTextField.setText(client.getName());
        VBox nameVBox = new VBox();

        nameVBox.setAlignment(Pos.CENTER_LEFT);
        nameVBox.getChildren().addAll(nameLabel, nameTextField);


        //Address Field
        Label addressLabel = new Label("Address");
        TextField addressTextField = new TextField();
        addressTextField.setText(client.getAddress());
        VBox addressVBox = new VBox();

        addressLabel.setAlignment(Pos.CENTER_LEFT);
        addressVBox.getChildren().addAll(addressLabel, addressTextField);


        //Phone Number Field
        Label phoneLabel = new Label("Phone Number");
        TextField phoneTextField = new TextField();
        phoneTextField.setText(client.getPhoneNumber());
        VBox phoneVBox = new VBox();

        phoneVBox.setAlignment(Pos.CENTER_LEFT);
        phoneVBox.getChildren().addAll(phoneLabel, phoneTextField);

        //Email Address Field
        Label emailLabel = new Label("Email Address");
        TextField emailTextField = new TextField();
        emailTextField.setText(client.getEmailAddress());
        VBox emailVbox = new VBox();

        emailVbox.setAlignment(Pos.CENTER_LEFT);
        emailVbox.getChildren().addAll(emailLabel, emailTextField);

        //CNP Filed
        Label cnpLabel = new Label("CNP");
        TextField cnpTextField = new TextField();
        cnpTextField.setText(client.getCnp());
        cnpTextField.setPromptText("CNP");
        VBox cnpVbox = new VBox();

        cnpVbox.setAlignment(Pos.CENTER_LEFT);
        cnpVbox.getChildren().addAll(cnpLabel, cnpTextField);

        //Gdpr Field
        Label gdprLabel = new Label("Gdpr");
        gdprLabel.setFont(new Font("Arial", 20));
        CheckBox gdprCheckBox = new CheckBox();
        gdprCheckBox.setSelected(client.isGdpr());
        gdprCheckBox.setPrefSize(50, 50);
        //Lead Field
        Label leadLabel = new Label("Lead");
        leadLabel.setFont(new Font("Arial", 20));
        CheckBox leadCheckBox = new CheckBox();
        leadCheckBox.setSelected(client.isLead());
        leadCheckBox.setPrefSize(50, 50);

        HBox gdprHBox = new HBox();
        gdprHBox.setSpacing(10);
        gdprHBox.setAlignment(Pos.CENTER);
        gdprHBox.getChildren().addAll(gdprLabel, gdprCheckBox, leadLabel, leadCheckBox);


        //Update Button
        Button updateButton = new Button("Update");
        updateButton.setPrefWidth(100);
        updateButton.setPrefHeight(50);
        updateButton.setOnMousePressed(event2 -> {
            if (cnpTextField.getText().length() != 13) {
                cnpTextField.setStyle("-fx-background-color: red;");
            } else if (phoneTextField.getText().length() != 10) {
                phoneTextField.setStyle("-fx-background-color: red;");
            } else {
                clientService.updateClient(client, nameTextField.getText(), addressTextField.getText(),
                        phoneTextField.getText(), emailTextField.getText(), cnpTextField.getText(),
                        gdprCheckBox.isSelected(), leadCheckBox.isSelected());
                updateButton.setOnMouseReleased(event3 -> updateWindow.close());
            }
        });


        VBox vBox = new VBox();
        vBox.setSpacing(35);
        vBox.setPadding(new Insets(30, 20, 20, 20));

        vBox.getChildren().addAll(nameVBox, addressVBox, phoneVBox, emailVbox, cnpVbox, gdprHBox, updateButton);
        vBox.setAlignment(Pos.CENTER);
        Scene scene = new Scene(vBox);
        scene.getStylesheets().add("Viper.css");
        updateWindow.setScene(scene);
        updateWindow.showAndWait();


    }

}

