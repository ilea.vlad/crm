package com.practicalProject.test.view;

import com.practicalProject.test.model.Client;
import com.practicalProject.test.service.ReminderService;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;


public class AddReminderView {
    ReminderService reminderService = new ReminderService();

    public void addReminder(Client client){
        Stage addReminderWindow = new Stage();
        addReminderWindow.setHeight(700);
        addReminderWindow.setWidth(400);
        addReminderWindow.initModality(Modality.APPLICATION_MODAL);

        Label clientName = new Label(client.getName());

        clientName.setFont(new Font("Arial",20));
        DatePicker reminderDate = new DatePicker();
        TextArea messageTextArea = new TextArea();
        messageTextArea.setPrefSize(300,200);
        Button addReminderButton = new Button("Add Reminder");

        addReminderButton.setOnAction(event -> {
            reminderService.addReminder(client,messageTextArea.getText(),reminderDate.getValue());
            addReminderButton.setOnMouseReleased(event1 -> {
                addReminderWindow.close();
            });
        });

        VBox addMessageVbox = new VBox();
        addMessageVbox.setSpacing(50);
        addMessageVbox.setAlignment(Pos.CENTER);
        addMessageVbox.setPadding(new Insets(10,50,20,50));
        addMessageVbox.getChildren().addAll(clientName,reminderDate,messageTextArea,addReminderButton);

        Scene  scene = new Scene(addMessageVbox);
        scene.getStylesheets().add("Viper.css");
        addReminderWindow.setScene(scene);
        addReminderWindow.showAndWait();
    }
}
