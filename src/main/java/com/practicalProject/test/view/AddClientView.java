package com.practicalProject.test.view;



import com.practicalProject.test.service.ClientService;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;


public class AddClientView {
    ClientService clientService = new ClientService();


    public void addClient() {
        Stage addClientWindow = new Stage();
        addClientWindow.setHeight(700);
        addClientWindow.setWidth(400);
        addClientWindow.initModality(Modality.APPLICATION_MODAL);


        //Name field
        Label nameLabel = new Label("Name");
        TextField nameTextField = new TextField();
        nameTextField.setPromptText("Name");
        VBox nameVBox = new VBox();

        nameVBox.setAlignment(Pos.CENTER_LEFT);
        nameVBox.getChildren().addAll(nameLabel, nameTextField);


        //Address Field
        Label addressLabel = new Label("Address");
        TextField addressTextField = new TextField();
        addressTextField.setPromptText("Address");
        VBox addressVBox = new VBox();

        addressLabel.setAlignment(Pos.CENTER_LEFT);
        addressVBox.getChildren().addAll(addressLabel, addressTextField);


        //Phone Number Field
        Label phoneLabel = new Label("Phone Number");
        TextField phoneTextField = new TextField();
        phoneTextField.setPromptText("Phone Number");
        VBox phoneVBox = new VBox();

        phoneVBox.setAlignment(Pos.CENTER_LEFT);
        phoneVBox.getChildren().addAll(phoneLabel, phoneTextField);

        //Email Address Field
        Label emailLabel = new Label("Email Address");
        TextField emailTextField = new TextField();
        emailTextField.setPromptText("Email Address");
        VBox emailVbox = new VBox();

        emailVbox.setAlignment(Pos.CENTER_LEFT);
        emailVbox.getChildren().addAll(emailLabel, emailTextField);

        //CNP Filed
        Label cnpLabel = new Label("CNP");
        TextField cnpTextField = new TextField();
        cnpTextField.setPromptText("CNP");
        VBox cnpVbox = new VBox();


        cnpVbox.setAlignment(Pos.CENTER_LEFT);
        cnpVbox.getChildren().addAll(cnpLabel, cnpTextField);

        //Gdpr Field
        Label gdprLabel = new Label("Gdpr");
        gdprLabel.setFont(new Font("Arial", 20));
        CheckBox gdprCheckBox = new CheckBox();
        gdprCheckBox.setPrefSize(50, 50);

        //Lead Field
        Label leadLabel = new Label("Lead");
        leadLabel.setFont(new Font("Arial", 20));
        CheckBox leadCheckBox = new CheckBox();
        leadCheckBox.setPrefSize(50, 50);

        HBox gdprHBox = new HBox();
        gdprHBox.setSpacing(10);
        gdprHBox.setAlignment(Pos.CENTER);
        gdprHBox.getChildren().addAll(gdprLabel, gdprCheckBox, leadLabel, leadCheckBox);

        //Add Button
        Button addButton = new Button("Add");
        addButton.setPrefWidth(100);
        addButton.setPrefHeight(50);

        addButton.setOnMousePressed(event -> {
            if (cnpTextField.getText().length() != 13) {
                cnpTextField.setStyle("-fx-background-color: red;");
            } else if (phoneTextField.getText().length() != 10) {
                phoneTextField.setStyle("-fx-background-color: red;");
            } else {
                clientService.createClient(nameTextField.getText(), addressTextField.getText(), phoneTextField.getText(), emailTextField.getText(), cnpTextField.getText(), gdprCheckBox.isSelected(), leadCheckBox.isSelected());
                addButton.setOnMouseReleased(event1 -> addClientWindow.close());
            }
        });

        VBox vBox = new VBox();
        vBox.setSpacing(35);
        vBox.setPadding(new Insets(30, 20, 20, 20));
        vBox.setAlignment(Pos.CENTER);
        vBox.getChildren().addAll(nameVBox, addressVBox, phoneVBox, emailVbox, cnpVbox, gdprHBox, addButton);

        Scene scene = new Scene(vBox);
        scene.getStylesheets().add("Viper.css");
        addClientWindow.setScene(scene);
        addClientWindow.showAndWait();

    }
}