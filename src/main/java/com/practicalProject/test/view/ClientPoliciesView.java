package com.practicalProject.test.view;

import com.practicalProject.test.model.Client;
import com.practicalProject.test.model.ClientPolicy;
import com.practicalProject.test.model.Policy;
import com.practicalProject.test.repositoty.ClientPolicyRepository;
import com.practicalProject.test.service.ClientPolicyService;
import com.sun.webkit.PolicyClient;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;


import javax.persistence.criteria.CriteriaBuilder;
import java.text.SimpleDateFormat;
import java.util.Date;


public class ClientPoliciesView {
private ClientPolicyService clientPolicyService = new ClientPolicyService();
private ObservableList clientPolicyObservableList;
private UpdatePolicyView updatePolicyView = new UpdatePolicyView();



    public void ViewClientPolicies(Client client) {
        clientPolicyObservableList = FXCollections.observableList(clientPolicyService.getPoliciesByClient(client));
        BorderPane borderPane = new BorderPane();
        borderPane.setPrefSize(1070, 800);
        Stage showPolicyStage = new Stage();
        showPolicyStage.initModality(Modality.APPLICATION_MODAL);
        showPolicyStage.setHeight(800);
        showPolicyStage.setWidth(1070);
        Label label = new Label(client.getName());
        label.setFont(new Font("Arial", 45));
        label.setPrefSize(500, 48);


        //Create buttons
        Button updatePolicyButton = new Button("Update Policy");
        updatePolicyButton.setPrefWidth(150);
        updatePolicyButton.setPrefHeight(100);
        Button addPolicyButton = new Button("Add Policy");
        addPolicyButton.setPrefWidth(150);
        addPolicyButton.setPrefHeight(100);

        VBox buttonsVbox = new VBox();
        buttonsVbox.setAlignment(Pos.CENTER);
        buttonsVbox.setSpacing(30);
        buttonsVbox.setPadding(new Insets(0, 90, 0, 100));
        buttonsVbox.getChildren().addAll(updatePolicyButton, addPolicyButton);

        HBox labelHbox = new HBox();
        labelHbox.setAlignment(Pos.TOP_LEFT);
        labelHbox.setPadding(new Insets(20, 0, 30, 10));
        labelHbox.getChildren().add(label);

        TableView clientPolicyTableView = new TableView();
        clientPolicyTableView.setPrefSize(507, 427);
        TableColumn<ClientPolicy, Policy> policyNameCol = new TableColumn<>("Policy Name");
        policyNameCol.setMinWidth(200);
        TableColumn<ClientPolicy, String> policyNumberCol = new TableColumn<>("Policy Number");
        policyNumberCol.setMinWidth(100);
        TableColumn<ClientPolicy, Integer> periodCol = new TableColumn<>("Period");
        periodCol.setMinWidth(30);
        TableColumn<ClientPolicy, Date> startDateCol = new TableColumn<>("Start Date");
        startDateCol.setMinWidth(100);
        TableColumn<ClientPolicy, Date> endDateCol = new TableColumn<>("End Date");
        endDateCol.setMinWidth(100);
        TableColumn<ClientPolicy, Integer> feeCol = new TableColumn<>("RON");
        feeCol.setMinWidth(70);
        TableColumn<ClientPolicy,Double> euroValCol = new TableColumn<>("EUR");
        feeCol.setMinWidth(70);


        policyNameCol.setCellValueFactory(new PropertyValueFactory<>("policy"));
        policyNumberCol.setCellValueFactory(new PropertyValueFactory<>("policyNumber"));
        periodCol.setCellValueFactory(new PropertyValueFactory<>("period"));
        startDateCol.setCellValueFactory(new PropertyValueFactory<>("startDate"));
        endDateCol.setCellValueFactory(new PropertyValueFactory<>("endDate"));
        feeCol.setCellValueFactory(new PropertyValueFactory<>("fee"));
        euroValCol.setCellValueFactory(new PropertyValueFactory<>("euroPrice"));
        clientPolicyTableView.setItems(clientPolicyObservableList);
        clientPolicyTableView.getColumns().addAll(policyNameCol, policyNumberCol, periodCol, startDateCol, endDateCol, feeCol,euroValCol);
        clientPolicyTableView.setEditable(false);
        updatePolicyButton.setOnMousePressed(event -> {

        });

        addPolicyButton.setOnAction(event -> {
            AddPolicyView addPolicyView = new AddPolicyView();
            try {
                addPolicyView.addPolicy(client);

            } catch (Exception e) {
                e.printStackTrace();
            }


        });
        addPolicyButton.setOnMouseReleased(event1 -> {
            clientPolicyObservableList = FXCollections.observableList(clientPolicyService.getPoliciesByClient(client));
            clientPolicyTableView.setItems(clientPolicyObservableList);
        });



        clientPolicyTableView.setOnMouseClicked(event -> {

            ClientPolicy clientPolicy =(ClientPolicy) clientPolicyTableView.getSelectionModel().getSelectedItem();
            updatePolicyButton.setOnAction(event1 -> { updatePolicyView.updatePolicy(clientPolicy); });
            updatePolicyButton.setOnMouseReleased(event1 -> {
                clientPolicyObservableList = FXCollections.observableList(clientPolicyService.getPoliciesByClient(client));
                clientPolicyTableView.setItems(clientPolicyObservableList);
            });

        });


        borderPane.setTop(labelHbox);
        borderPane.setLeft(buttonsVbox);
        borderPane.setCenter(clientPolicyTableView);


        Scene scene = new Scene(borderPane);
        scene.getStylesheets().add("Viper.css");
        showPolicyStage.setScene(scene);
        showPolicyStage.showAndWait();


    }

}
