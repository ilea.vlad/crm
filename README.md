Customer Relationship Management (CRM) Software made with Hibernate, MySql. This Software is a mini version which has the following features -

-Add/Update clients
-Add/Update policy for clients
-Add reminders
-Sending email with client birthday,reminder on current day and the policy who expired in next 10 days


The Software is made using following Technologies -

-Java - Programming Language.
-JavaFX - used for interface
-Hibernate.
-MySQL - Database.

Setup

The Project can be setup locally with the following steps.

Use git clone to clone this repo to your local machine:


       $ git clone https://gitlab.com/ilea.vlad/crm

      
The Project uses MySQL for database so install MySQL server into your local machine.
Use Username - root and Password - admin or change the following lines in the hibernate.cfg file.

      <property name = "hibernate.connection.username">
            root
        </property>

        <property name = "hibernate.connection.password">
            admin
        </property>
        
###Create database and necessary Tables in the crm Database
This resources folder contains a sql file called Create_database.sql which contains all the sql queries for creating Tables.

So everything is setup, open the project in IntelliJ and run MainView class from view package.

